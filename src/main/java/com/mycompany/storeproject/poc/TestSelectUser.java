/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

//import com.oracle.jrockit.jfr.Producer;
import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author Yumat
 */
public class TestSelectUser {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, tel,salary,status,address,role, password FROM employee";
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                double salary = result.getDouble("salary");
                String status = result.getString("status");
                String address = result.getString("address");
                String role = result.getString("role");
                String password = result.getString("password");
                Employee user = new Employee(id,name,tel,status,salary,address,role,password);
                System.out.println(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
