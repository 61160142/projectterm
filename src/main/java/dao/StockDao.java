/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Stock;

public class StockDao implements DaoInterface<Stock> {

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO stock (name,type,status,quantity) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getType());
            stmt.setString(3, object.getStatus());
            stmt.setInt(4, object.getQuantity());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            //Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,type,status,quantity FROM stock";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String type = result.getString("type");
                String status = result.getString("status");
                int quantity = result.getInt("quantity");
//                String img = result.getString("name")+".png";
                Stock stock = new Stock(id, name, type, status, quantity);
                list.add(stock);
            }
        } catch (SQLException ex) {
            //Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    public ArrayList<Stock> getIndex(int i) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,type,status,quantity FROM stock where id=" + i;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String type = result.getString("type");
                String status = result.getString("status");
                int quantity = result.getInt("quantity");
                Stock stock = new Stock(id, name, type, status, quantity);
                list.add(stock);
            }
        } catch (SQLException ex) {
            //Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,type,status,quantity FROM stock WHERE id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String type = result.getString("type");
                String status = result.getString("status");
                int pquantity = result.getInt("quantity");
                Stock stock = new Stock(pid, name, type, status, pquantity);
                return stock;
            }
        } catch (SQLException ex) {
            //Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM stock WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE stock SET name = ?,type = ?,status = ?,quantity = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getType());
            stmt.setString(3, object.getStatus());
            stmt.setInt(4, object.getQuantity());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        StockDao dao = new StockDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Stock(-1, "coffeeBean", "ingredian", "in stock", 40));
        System.out.println("id: " + id);
        Stock lastStock = dao.get(id);
        System.out.println("list stock: "+lastStock);
        lastStock.setStatus("out off stock");
        int row = dao.update(lastStock);
        Stock updateStock = dao.get(id);
        System.out.println("update stock: "+updateStock);
        dao.delete(id);
        Stock deleteStock = dao.get(id);
        System.out.println("delete stock: "+deleteStock);

    }
}
