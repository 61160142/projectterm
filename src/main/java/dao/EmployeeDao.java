/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestSelectEmployee;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author Yumat
 */
public class EmployeeDao implements DaoInterface<Employee> {
     private static String role;
    private static int empId;

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO employee (name,tel,status,salary,address,role,password) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getStatus());
            stmt.setDouble(4, object.getSalary());
            stmt.setString(5, object.getAddress());
            stmt.setString(6, object.getRole());
            stmt.setString(7, object.getPassword());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,status,salary,address,role,password FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String status = result.getString("status");
                String address = result.getString("address");
                double salary = result.getDouble("salary");
                String role = result.getString("role");
                String password = result.getString("password");
                Employee employee = new Employee(id, name, tel, status, salary,address,role,password);
                list.add(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,status,salary,address,role,password FROM employee WHERE id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String status = result.getString("status");
                double salary = result.getDouble("salary");
                String address = result.getString("address");
                String role = result.getString("role");
                String password = result.getString("password");
                Employee employee = new Employee(id, name, tel, status, salary,address,role,password);
                
                return employee;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }
    public ArrayList<Employee> getTel(String t) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,status,salary,address,role,password FROM employee WHERE tel = \""+t+"\"";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String status = result.getString("status");
                double salary = result.getDouble("salary");
                String address = result.getString("address");
                String role = result.getString("role");
                String password = result.getString("password");
                Employee employee = new Employee(eid, name, tel, status, salary,address,role,password);
                list.add(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM employee WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE employee SET name = ?,tel = ?, status = ?, salary = ?, address = ? , role = ? ,password = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getStatus());
            stmt.setDouble(4, object.getSalary());
            stmt.setString(5, object.getAddress());
            stmt.setString(6, object.getRole());
            stmt.setString(7, object.getPassword());
            stmt.setInt(8, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Employee(-1, "Genshin", "0966666666","off",400.00,"chon","Employee","1234"));
        System.out.println("id: " + id);
        Employee lastEmployee = dao.get(id);
        System.out.println("list employee: "+lastEmployee);
        lastEmployee.setPassword("0896969696");
        int row = dao.update(lastEmployee);
        Employee updateEmployee = dao.get(id);
        System.out.println("update employee: "+updateEmployee);
        
    }
    public Employee getByUserId(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,status,salary,address,role,password FROM employee WHERE id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String status = result.getString("status");
                double salary = result.getDouble("salary");
                String address = result.getString("address");
                String role = result.getString("role");
                String password = result.getString("password");
                Employee employee = new Employee(id, name, tel, status, salary,address,role,password);
                return employee;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }
    public static boolean auth(String loginName, String password) {
        boolean temp = false;
        EmployeeDao dao = new EmployeeDao();
        ArrayList<Employee> a = dao.getAll();
        for (int i=0;i<a.size();i++) {
            if ( a.get(i).getName().equals(loginName)&&a.get(i).getPassword().equals(password)&& a.get(i).getStatus().equals("Working")) {
                role = a.get(i).getRole();
                empId = a.get(i).getId();
                System.out.println(role);
                temp = true;
                break;
            } else {
                temp = false;   
            }
        }
        return temp;
    }
        public static int authId(String loginName, String password) {
        int temp = 0;
        EmployeeDao dao = new EmployeeDao();
        ArrayList<Employee> a = dao.getAll();
        for (int i=0;i<a.size();i++) {
            if ( a.get(i).getName().equals(loginName)&&a.get(i).getPassword().equals(password)&& a.get(i).getStatus().equals("Working")) {
                role = a.get(i).getRole();
                empId = a.get(i).getId();
                temp = a.get(i).getId();
                break;
            } else {
                temp = 0;   
            }
        }
        return temp;
    }
        public static String getRole() {
        return role;
    }
            public static int getId() {
        return empId;
    }

    public ArrayList<Employee> getIndex(int id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,status,salary,address,role,password FROM employee WHERE id = "+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String status = result.getString("status");
                double salary = result.getDouble("salary");
                String address = result.getString("address");
                String role = result.getString("role");
                String password = result.getString("password");
                Employee employee = new Employee(eid, name, tel, status, salary,address,role,password);
                list.add(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

        
}
