/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestSelectCustomer;
import com.mycompany.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Employee;
import model.Order;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author Yumat
 */
public class OrderDao implements DaoInterface<Order> {

    @Override
    public int add(Order object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO [order] (customer_id, employee_id, total)VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (OrderDetail r : object.getOrderDetail()) {
                String sqlDetail = "INSERT INTO order_detail (order_id,product_id,price,amount)VALUES (?,?,?,? );";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getOrder().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt");
            //Logger.getLogger(TestSelectReceipt.class.getName()).log(Level.SEVERE, null, ex);

        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Order> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT  r.id as id,\n"
                    + "       r.created as created,\n"
                    + "       r.customer_id as customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       r.employee_id as employee_id,\n"
                    + "       u.name as employee_name,\n"
                    + "       u.tel as employee_tel,\n"
                    + "       r.total as total\n"
                    + "  FROM [order] r, customer c, employee u\n"
                    + "  WHERE r.customer_id = c.id AND r.employee_id = u.id"
                    + "  ORDER BY created DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int employeeId = result.getInt("employee_id");
                String employeeName = result.getString("employee_name");
                String employeeTel = result.getString("employee_tel");
                double total = result.getDouble("total");
                Order order = new Order(id, created,
                        new Employee(employeeId, employeeName, employeeTel),
                        new Customer(customerId, customerName, customerTel));
                list.add(order);
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to selsct all receipt!!" + ex.getMessage());
            //Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt!!" + ex.getMessage());
        }
        db.close();
        return list;
    }

    public ArrayList<OrderDetail> getOrderDetail() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, order_id, product_id, price, amount FROM order_detail ORDER BY id DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int order_id = result.getInt("order_id");
                int product_id = result.getInt("product_id");
                double price = result.getDouble("price");
                int amount = result.getInt("amount");
                ProductDao pDao = new ProductDao();
                OrderDao oDao = new OrderDao();
                OrderDetail order = new OrderDetail(id, pDao.get(product_id), amount, price, oDao.get(order_id));
                list.add(order);
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to selsct all receipt!!" + ex.getMessage());
            //Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
    public ArrayList<OrderDetail> getOrderDetailById(int detailId) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, order_id, product_id, price, amount FROM order_detail WHERE order_id = "+detailId+" ORDER BY id DESC ;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int order_id = result.getInt("order_id");
                int product_id = result.getInt("product_id");
                double price = result.getDouble("price");
                int amount = result.getInt("amount");
                ProductDao pDao = new ProductDao();
                OrderDao oDao = new OrderDao();
                OrderDetail order = new OrderDetail(id, pDao.get(product_id), amount, price, oDao.get(order_id));
                list.add(order);
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to selsct all receipt!!" + ex.getMessage());
            //Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    public ArrayList<Order> getTel(String t) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
//            try {
//                String sql = "SELECT id,name,tel,point,status FROM customer where tel =\""+t+"\"";
//                Statement stmt = conn.createStatement();
//                ResultSet result = stmt.executeQuery(sql);
//                if (result.next()) {
//                    int id = result.getInt("id");
//                    String name = result.getString("name");
//                    String tel = result.getString("tel");
//                    int point = result.getInt("point");
//                    String status = result.getString("status");
//                    Order customer = new Order(id, name, tel,point,status);
//                    list.add(customer);
//                }
//            } catch (SQLException ex) {
//                Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
//            }
//    
//            db.close();
        return list;
    }

    @Override
    public Order get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n"
                    + "       created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       employee_id,\n"
                    + "       u.name as employee_name,\n"
                    + "       u.tel as employee_tel,\n"
                    + "       total\n"
                    + "  FROM [order] r, customer c, Employee u\n"
                    + "  WHERE r.id = ? AND r.customer_id = c.id AND r.employee_id = u.id;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int rid = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int employeeId = result.getInt("employee_id");
                String employeeName = result.getString("employee_name");
                String employeeTel = result.getString("employee_tel");
                double total = result.getDouble("total");
                Order order = new Order(rid, created,
                        new Employee(employeeId, employeeName, employeeTel),
                        new Customer(customerId, customerName, customerTel));
                getOrderDetail(conn, id, order);
                return order;
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to selsct order id " + id + "!!" + ex.getMessage());
            //Logger.getLogger(TestSelectReceipt.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing id " + id + " order!!" + ex.getMessage());
            //Logger.getLogger(OrderDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void getOrderDetail(Connection conn, int id, Order order) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n"
                + "       order_id,\n"
                + "       product_id,\n"
                + "       p.name as product_name,\n"
                + "       p.price as product_price,\n"
                + "       rd.price as price,\n"
                + "       amount\n"
                + "  FROM order_detail rd, product p\n"
                + "  WHERE order_id = ? AND rd.product_id = p.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int orderId = resultDetail.getInt("id");
            int productId = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double productprice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(productId, productName, productprice);
            order.addOrderDetail(orderId, product, amount, price);
        }
    }
    public int getLastId() {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,created, customer_id, employee_id, total FROM [order] ORDER BY id DESC LIMIT 1;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int created = result.getInt("created");
                int customer_id = result.getInt("customer_id");
                double employee_id = result.getDouble("employee_id");
//                ProductDao pDao = new ProductDao();
//                OrderDao oDao = new OrderDao();
//                Order order = new OrderDetail(id, created, customer_id, employee_id);
                db.close();
                return id;
            }

        } catch (SQLException ex) {
            System.out.println("Error: Unable to selsct all receipt!!" + ex.getMessage());
            //Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return 0;
    }
    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM order WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete order id " + id + "!!");
            //Logger.getLogger(TestSelectReceipt.class.getName()).log(Level.SEVERE, null, ex);

        }
        db.close();
        return row;
    }

    @Override
    public int update(Order object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
//            try {
//                String sql = "UPDATE customer SET name = ?,tel = ?,point = ?,status = ? WHERE id = ?";
//                PreparedStatement stmt = conn.prepareStatement(sql);
//                stmt.setString(1, object.getName());
//                stmt.setString(2, object.getTel());
//                stmt.setInt(3, object.getPoint());
//                stmt.setString(4, object.getStatus());
//                stmt.setInt(5, object.getId());
//                row = stmt.executeUpdate();
//            } catch (SQLException ex) {
//                Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
//            }
//    
//            db.close();
        return row;
    }

    public static void main(String[] args) {
        OrderDao dao = new OrderDao();
        Employee seller = new Employee(1, "jentle", "0822222222");
        Customer customer = new Customer(1, "taeyeon kim", "0833333333");
        Order order = new Order(seller, customer);
        Product p1 = new Product(1, "Americano", 30);
//        Product p2 = new Product(2, "Oh Leing 1", 30);
        order.addOrderDetail(p1, 1);
//        order.addOrderDetail(p2, 3);
        dao.add(order);

//        Order newOrder = dao.get(order.getId());
//        System.out.println("New Order: " + newOrder);
    }

}
