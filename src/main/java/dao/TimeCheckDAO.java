/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestSelectCustomer;
import com.mycompany.storeproject.poc.TestSelectEmployee;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Employee;
import model.TimeCheck;
import panel.MainStaffFrame;

/**
 *
 * @author Yumat
 */
public class TimeCheckDAO implements DaoInterface<TimeCheck> {

    private static String role;

    @Override
    public int add(TimeCheck object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO time_check (employee_id,time_in) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getEmpId());
            stmt.setString(2, object.getInTime());
            //stmt.setString(4, object.getOutTime());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<TimeCheck> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT employee_id,id,time_in,time_out FROM time_check";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int empid = result.getInt("employee_id");
                int id = result.getInt("id");
                String timeIn = result.getString("time_in");
                String timeOut = result.getString("time_out");
                TimeCheck time = new TimeCheck(id, empid, timeIn, timeOut);
                list.add(time);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    public ArrayList<TimeCheck> getAllByID(int empId) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT employee_id,id,time_in,time_out FROM time_check WHERE employee_id =" + empId;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int empid = result.getInt("employee_id");
                String timeIn = result.getString("time_in");
                String timeOut = result.getString("time_out");
                TimeCheck time = new TimeCheck(id, empid, timeIn, timeOut);
                list.add(time);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    public ArrayList<TimeCheck> getLast() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT employee_id,id,time_out FROM time_check";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int empid = result.getInt("employee_id");
                int id = result.getInt("id");
                String timeout = result.getString("time_out");
                TimeCheck time = new TimeCheck(empid, id, timeout);
                list.add(time);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    public TimeCheck get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,employee_id,time_in,time_out FROM time_check WHERE id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                int empid = result.getInt("employee_id");
                String timein = result.getString("time_in");
                String timeout = result.getString("time_out");
                TimeCheck mainmenu = new TimeCheck(pid, empid, timein, timeout);
                return mainmenu;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int update(int empWorkingId, String checkOutTime, int lastTime) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE time_check SET employee_id = ?,time_out = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, empWorkingId);
            stmt.setString(2, checkOutTime);
            stmt.setInt(3, lastTime);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object get() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] args) {
        TimeCheckDAO dao = new TimeCheckDAO();
//        System.out.println(dao.getAll());
        int in = dao.add(new TimeCheck(2, "TEst"));
        System.out.println(in);
    }

    @Override
    public int update(TimeCheck object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
