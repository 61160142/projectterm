/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestSelectCustomer;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Employee;

/**
 *
 * @author Yumat
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO customer (name,tel,point,status) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getPoint());
            stmt.setString(4, object.getStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,point,status FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                int point = result.getInt("point");
                String status = result.getString("status");
                Customer customer = new Customer(id, name, tel,point,status);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }
    public ArrayList<Customer> getTel(String t) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,point,status FROM customer where tel =\""+t+"\"";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                int point = result.getInt("point");
                String status = result.getString("status");
                Customer customer = new Customer(id, name, tel,point,status);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,point,status FROM customer WHERE id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                int point = result.getInt("point");
                String status = result.getString("status");
                Customer customer = new Customer(pid, name, tel,point,status);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public int getPoint(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,point,status FROM customer WHERE id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                int point = result.getInt("point");
                String status = result.getString("status");
                Customer customer = new Customer(pid, name, tel,point,status);
                return customer.getPoint();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM customer WHERE id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public int updatePoint(int cusId,int point) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET point = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, point);
            stmt.setInt(2, cusId);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
//        CustomerDao dao = new CustomerDao();
//        System.out.println(dao.getAll());
//        System.out.println(dao.get(1));
//        int id = dao.add(new Customer(-1, "Genshin", "0966666666",41,"y"));
//        System.out.println("id: " + id);
//        Customer lastCustomer = dao.get(id);
//        System.out.println("list product: "+lastCustomer);
//        lastCustomer.setStatus("n");
//        int row = dao.update(lastCustomer);
//        Customer updateCustomer = dao.get(id);
//        System.out.println("update product: "+updateCustomer);
//        dao.delete(id);
//        Customer deleteCustomer = dao.get(id);
//        System.out.println("delete product: "+deleteCustomer);

    }
    public static String auth(String tel) {
        String temp = "Unknow";
        CustomerDao dao = new CustomerDao();
        ArrayList<Customer> array = dao.getAll();
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i).getTel().equals(tel)) {
                temp = array.get(i).getName();
                break;
            } else {
                temp = "Unknow";
            }
        }
        return temp;
    }
        public static int authID(String tel) {
        int temp = 0;
        CustomerDao dao = new CustomerDao();
        ArrayList<Customer> array = dao.getAll();
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i).getTel().equals(tel)) {
                temp = array.get(i).getId();
                break;
            } else {
                temp = 0;
            }
        }
        return temp;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET name = ?,tel = ?,point = ?,status = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getPoint());
            stmt.setString(4, object.getStatus());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }


}
