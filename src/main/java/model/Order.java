/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Yumat
 */
public class Order {

    private int id;
    private Date created;
    private Employee seller;
    private Customer customer;
    private ArrayList<OrderDetail> orderDetail;
    private double total;

    public Order(int id, Date created, Employee seller, Customer customer) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.customer = customer;
        orderDetail = new ArrayList<>();
    }

    public Order(Employee seller, Customer customer) {
        this(-1, null, seller, customer);
    }

    public void clear() {
        orderDetail.clear();
    }

    public void addOrderDetail(int id, Product product, int amount, double price) {
        for (int row = 0; row < orderDetail.size(); row++) {
            OrderDetail r = orderDetail.get(row);
            if (r.getProduct().getId() == product.getId()) {
                r.addAmount(amount);
                return;
            }
        }
        total = amount * price;
        orderDetail.add(new OrderDetail(id, product, amount, price, this));
    }

    public void clear(int row) { 
       orderDetail.remove(row-1);
    }


    public void addOrderDetail(Product product, int amount) {
        addOrderDetail(-1, product, amount, product.getPrice());
    }

    public void addOrdertDetail(Product product, int amount) {
        addOrderDetail(-1, product, amount, product.getPrice());
    }

    public void deleteOrderDetail(int row) {
        orderDetail.remove(row);
    }

    public double getTotal() {
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Employee getSeller() {
        return seller;
    }

    public void setSeller(Employee seller) {
        this.seller = seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<OrderDetail> getOrderDetail() {
        return orderDetail;
    }

    public void setReceiptDetail(ArrayList<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", created=" + created + ", seller=" + seller + ", customer=" + customer + ", orderDetail=" + orderDetail + ", total=" + total + '}';
    }

}
