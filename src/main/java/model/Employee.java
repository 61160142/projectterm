/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nitro 5
 */
public class Employee {

    private int id;
    private String name;
    private String tel;
    private String status;
    private Double salary;
    private String address;
    private String role;
    private String password;

    public Employee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", status=" + status + ", salary=" + salary + ", address=" + address + ", role=" + role + ", password=" + password + '}';
    }

    public Employee(int id, String name, String tel, String status, Double salary, String address, String role, String password) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.status = status;
        this.salary = salary;
        this.address = address;
        this.role = role;
        this.password = password;
    }
    public Employee(int id, String name, String tel){
        this(-1,name,tel,"",0.0,"","","");
    }
    public Employee(String name, String tel){
        this(-1,name, tel, "",0.0,"","","");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password.length() >= 6) {
            this.password = password;
        }
    }

}
