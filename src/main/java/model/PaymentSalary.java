/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author kittimasak
 */
public class PaymentSalary {

    private int id;
    private String name;
    private String role;
    private double salary;
    private double TotalSalary;

    public PaymentSalary(int id, String name, String role, double salary, double TotalSalary) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.salary = salary;
        this.TotalSalary = TotalSalary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getTotalSalary() {
        return TotalSalary;
    }

    public void setTotalSalary(double TotalSalary) {
        this.TotalSalary = TotalSalary;
    }

    @Override
    public String toString() {
        return "PaymentSalary{" + "id=" + id + ", name=" + name + ", role=" + role + ", salary=" + salary + ", TotalSalary=" + TotalSalary + '}';
    }
    

}
