/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Yumat
 */
public class OrderDetail {

    private int id;
    private Product product;
    private int amount;
    private double price;
    private int order_id;
    private int product_id;
    private double detailPrice;
    Order order;

    public OrderDetail(int id, Product product, int amount, double price, Order order) {
        this.id = id;
        this.product = product;
        this.amount = amount;
        this.price = price;
        this.order = order;
    }

    public OrderDetail(Product product, int amount, double price, Order order) {
        this(-1, product, amount, price, order);
    }

    public double getTotal() {
        return amount * product.getPrice();
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    void addAmount(int amount) {
        this.amount += amount;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "id= " + id + ", product= " + product + ", amount= " + amount + ", price= " + price + ", total= " + this.getTotal() + '}';
    }

    public int getOrder_id() {
        return order.getId();
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public double getProduct_id() {
        return product.getId();
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public double getDetailPrice() {
        return detailPrice;
    }

    public void setDetailPrice(double detailPrice) {
        this.detailPrice = detailPrice;
    }

}
