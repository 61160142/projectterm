/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import java.util.Date;
/**
 *
 * @author acer
 */
public class TimeCheck {

    Date today = new Date();
    private int id;
    private int empId;
    private String checkIn;
    private String checkOut;

    public TimeCheck(int id, int empId, String checkIn, String checkOut) {
        this.id = id;
        this.empId = empId;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
    }

    public TimeCheck(int emp_id,String in_time) {
        this.empId = emp_id;
        this.checkIn = in_time;
    }

    public TimeCheck(TimeCheck lastEmployee, String formattedDate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void update
        (int emp_id,String out_time) {
        this.empId = emp_id;
        this.checkOut = out_time;
    }

    public TimeCheck(int empid, int id, String timeout) {
        this.id = id;
        this.empId = empid;
        this.checkOut = timeout;
    }
    
    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getInTime() {
        return checkIn;
    }

    public void setInTime(String inTime) {
        this.checkIn = inTime;
    }

    public String getOutTime() {
        return checkOut;
    }

    public void setOutTime(String outTime) {
        this.checkOut = outTime;
    }


    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    @Override
    public String toString() {
        return "TimeCheck{" + "today=" + today + ", id=" + id + ", empId=" + empId + ", checkIn=" + checkIn + ", checkOut=" + checkOut + '}';
    }
    
}
